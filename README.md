![yttrim](.assets/banner.webp)

# yttrim

[{video} yttrim in action.](.assets/demo.mp4)

Trim portions from online videos with ease.

## CSVs for batch trim

[{video} Batch trimming with CSVs.](.assets/csv.mp4)

## Dependencies

- [yt-dlp](https://https://github.com/yt-dlp/yt-dlp) 
- [fzf](https://github.com/junegunn/fzf)
- [ffmpeg](https://ffmpeg.org)

## Usage

```
Usage: yttrim [OPTION]...

  -ss <timestamp>         start timestamp
  -to <timestamp>         end timestamp
  -f/--filename <name>    filename to be saved as
  -l/--link <link>        audio/video link
  -m/--audio-only         to download the audio only
  --csv-gen               generate a template CSV (for batch trim)
  --csv <file>            provide the csv file for batch trim
  -h/--help               display this help and exit
```

## Installation

*AUR:*
```
yay -S yttrim
```

*Manual:*

```
git clone https://codeberg.org/theooo/yttrim 
cd yttrim/
sudo make install
```

or

```
sudo curl -sL "https://codeberg.org/theooo/yttrim/raw/branch/main/yttrim" -o /usr/local/bin/yttrim 
sudo chmod +x /usr/local/bin/yttrim
```

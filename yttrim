#!/bin/bash

###########################################################
### YTTRIM ################################################

###########################################################
### VARIABLES #############################################

ff="ffmpeg -loglevel quiet -stats"
fzf="fzf -e --info=inline"

### FORMATTING ############################################

underline="\e[4m"
reset="\e[0m"

###########################################################
### FUNCTIONS #############################################

### HELP ##################################################

helpinfo() {
  cat << EOF
Usage: yttrim [OPTION]...
Trim portions from online videos with ease.

  -ss <timestamp>         start timestamp
  -to <timestamp>         end timestamp
  -f/--filename <name>    filename to be saved as
  -l/--link <link>        audio/video link
  -m/--audio-only         to download the audio only
  --csv-gen               generate a template CSV (for batch trim)
  --csv <file>            provide the csv file for batch trim
  -h/--help               display this help and exit

EOF
}

### IMP ###################################################

depcheck() {
  # add array thing
	for dep in {ffmpeg,fzf,yt-dlp}; do
    if [[ -z $(command -v "$dep") ]]; then
			printf "%s not found. Please install it.\n" "$dep" >&2
			exit 2
		fi
	done
	unset dep
}

### UMM ###################################################

decide() {
  case $audio_only in
    y) audiod ;;
    n) videod ;;
    *) printf "Invalid choice.\n";;
  esac
  
  unset $begin $end $filename $link $audio_only
}

ask_loop() {
  while [[ -z $link ]]; do read -rp "Enter link: " link; done
  while [[ -z $begin ]]; do read -rp "Enter beginning: " begin; done
  while [[ -z $end ]]; do read -rp "Enter end: " end; done
  while [[ -z $filename ]]; do read -rp "Enter filename: " filename; done
  while [[ -z $audio_only ]]; do read -rp "Audio only: [y/n] " audio_only; done

  decide
}

### CONVERSION AND STUFF ##################################

audiod() {
  $ff -ss "$begin" -to "$end" \
    -i "$(yt-dlp -f bestaudio -g "$link")" "$filename.m4a" && \
    printf "\nAudio extracted and saved as M4A.\n\n"
}

merge() {
  $ff -ss "$begin" -to "$end" -i "$(yt-dlp -f "$id" -g "$link")" \
      -ss "$begin" -to "$end" -i "$(yt-dlp -f bestaudio -g "$link")" \
      -shortest "$filename.mkv" && \
    printf "\nVideo extracted.\n"
}

videod() {
  qual=$(yt-dlp -F -q --no-colors "$link" | $fzf)
  [[ -z $qual ]] && printf "\nID not selected. Download aborted.\n\n" && exit 2
  id=${qual%% *}
  
  if [ -z "$(grep only <<< "$qual")" ]
  then 
    $ff -ss "$begin" -to "$end" -i "$(yt-dlp -f "$id" -g "$link")" "$filename.mp4" && \
      printf "\nVideo extracted and saved as MP4.\n"
   else
     merge
  fi
}

### CSV ###################################################

csv_gen() {
  printf "Link,Beginning,End,Filename,Audio_only\t[FIRST LINE IS IGNORED]\n\n" > ./input.csv && \
    printf "Template CSV generated.\n"
}

csv_parse() {
  for line in $(nl -s, <(tail -n+2 $csv_file))
  do
    IFS=',' read -r num link begin end filename audio_only <<< "$line"

    printf \
    "\n${underline}Num${reset} -> $num \
    \n${underline}Link${reset} -> $link \
    \n${underline}Beginning${reset} -> $begin \
    \n${underline}End${reset} -> $end \
    \n${underline}File${reset} -> $filename \
    \n${underline}Audio${reset} -> $audio_only\n"

    decide

  done
}

###########################################################

depcheck

while [ "$1" != "" ]; do
    case $1 in
    -ss) shift; begin=$1 ;; 
    -to) shift; end=$1 ;; 
    -f | --filename) shift; filename=$1 ;; 
    -l | --link) shift; link=$1 ;; 
    -m | --audio-only) audio_only=y ;; 
    --csv-gen) csv_gen && exit ;; 
    --csv) shift; csv_file=$1; csv_parse && exit ;;
    -h | --help) helpinfo && exit ;;
    *) printf "Invalid option.\n" && helpinfo && exit ;;
    esac
    shift
done

ask_loop
